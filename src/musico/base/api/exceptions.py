from __future__ import unicode_literals

from rest_framework import status, exceptions
from rest_framework.response import Response


def my_error_handler(exc, *args):
    """
    Returns the response that should be used for any given exception.
    By default we handle the REST framework `APIException`, and some new exception we created...
    Any unhandled exceptions may return `None`, which will cause a 500 error
    to be raised.
    """
    if isinstance(exc, exceptions.APIException):
        headers = {}
        if getattr(exc, 'auth_header', None):
            headers['WWW-Authenticate'] = exc.auth_header
        if getattr(exc, 'wait', None):
            headers['X-Throttle-Wait-Seconds'] = '%d' % exc.wait

        return Response({'error': exc.detail},
                        status=exc.status_code,
                        headers=headers)

    elif isinstance(exc, MusicoNotFound):
        return Response({"success": False, "message": MusicoNotFound.get_message(exc)},
                        status=status.HTTP_404_NOT_FOUND)

    elif isinstance(exc, MusicoPermissionDenied):
        return Response({"success": False, "message": MusicoPermissionDenied.get_message(exc)},
                        status=status.HTTP_403_FORBIDDEN)

    elif isinstance(exc, MusicoBadRequest):
        return Response({"success": False, "message": MusicoBadRequest.get_message(exc)},
                        status=status.HTTP_400_BAD_REQUEST)

    elif isinstance(exc, MusicoBadFormatDate):
        return Response({"success": False, "message": MusicoBadFormatDate.get_message(exc)},
                        status=status.HTTP_400_BAD_REQUEST)

    elif isinstance(exc, MusicoBadFormatNumber):
        return Response({"success": False, "message": MusicoBadFormatNumber.get_message(exc)},
                        status=status.HTTP_400_BAD_REQUEST)

    elif isinstance(exc, MusicoBadImageFile):
        return Response({"success": False, "message": MusicoBadImageFile.get_message(exc)},
                        status=status.HTTP_400_BAD_REQUEST)

    elif isinstance(exc, MusicoBadImageFormat):
        return Response({"success": False, "message": MusicoBadImageFormat.get_message(exc)},
                        status=status.HTTP_400_BAD_REQUEST)

    elif isinstance(exc, MusicoMissingRequest):
        return Response({"success": False, "message": MusicoMissingRequest.get_message(exc)},
                        status=status.HTTP_400_BAD_REQUEST)

    elif isinstance(exc, MusicoCreateFail):
        return Response({"success": False, "message": MusicoCreateFail.get_message(exc)},
                        status=status.HTTP_500_INTERNAL_SERVER_ERROR)

    elif isinstance(exc, PersonalCreateUserExp):
        return Response({"success": False, "message": "Cannot create user"},
                        status=status.HTTP_500_INTERNAL_SERVER_ERROR)

    elif isinstance(exc, PersonalUpdateUserExp):
        return Response({"success": False, "message": "Cannot update user"},
                        status=status.HTTP_500_INTERNAL_SERVER_ERROR)

    elif isinstance(exc, LoginAccountDisabled):
        return Response({"success": False, "message": LoginAccountDisabled.get_message(exc)},
                        status=status.HTTP_404_NOT_FOUND)

    elif isinstance(exc, LoginAccountNotActive):
        return Response({"success": False, "message": LoginAccountNotActive.get_message(exc)},
                        status=status.HTTP_404_NOT_FOUND)

    elif isinstance(exc, LoginWrong):
        return Response({"success": False, "message": LoginWrong.get_message(exc)},
                        status=status.HTTP_404_NOT_FOUND)

    elif isinstance(exc, LoginValidate):
        return Response({"success": False, "message": LoginValidate.get_message(exc)},
                        status=status.HTTP_404_NOT_FOUND)

    elif isinstance(exc, LoginEmailNotFound):
        return Response({"success": False, "message": LoginEmailNotFound.get_message(exc)},
                        status=status.HTTP_404_NOT_FOUND)

    elif isinstance(exc, JsonFormatExp):
        return Response({"success": False, "message": "extra_info must be a json string"},
                        status=status.HTTP_400_BAD_REQUEST)

    # Note: Unhandled exceptions will raise a 500 error.
    return None


class MusicoException(Exception):
    message = 'Exception'

    def __init__(self, message=None):
        if message:
            self.message = message

    def get_message(self):
        return self.message


class MusicoNotFound(MusicoException):
    message = 'Not found data'


class MusicoPermissionDenied(MusicoException):
    message = 'Permission Denied'


class MusicoBadRequest(MusicoException):
    message = 'Bad Request'


class MusicoBadFormatDate(MusicoException):
    message = 'Incorrect format parameter: Wrong date format, should be YYYY-MM-DD'


class MusicoMissingRequest(MusicoException):
    message = 'Missing Request'


class MusicoCreateFail(MusicoException):
    message = 'Create Failed'


class MusicoBadImageFile(MusicoException):
    message = 'Wrong image file'


class MusicoBadImageFormat(MusicoException):
    message = 'Wrong image format, should be jpg, png or jpeg'


class MusicoBadFormatNumber(MusicoException):
    message = 'Incorrect format parameter: Wrong number format'

class JsonFormatExp(Exception):
    pass

class PersonalCreateUserExp(Exception):
    pass


class PersonalUpdateUserExp(Exception):
    pass

class LoginAccountDisabled(MusicoException):
    message = 'User account is disabled'

class LoginAccountNotActive(MusicoException):
    message = 'User account not active'

class LoginWrong(MusicoException):
    message = 'Unable to log in with provided credentials'

class LoginValidate(MusicoException):
    message = 'Must include email and password'

class LoginEmailNotFound(MusicoException):
    message = 'Account with this email does not exists'
