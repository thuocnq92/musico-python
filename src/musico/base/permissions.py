from rest_framework.permissions import BasePermission, AllowAny
from django.conf import settings

VERIFY_CLIENT_API_KEY = 'HTTP_CLIENTAPIKEY'
HTTP_AUTHORIZATION = 'HTTP_AUTHORIZATION'


class IsAppVerified(AllowAny):
    """
    verify client api key
    """
    message = {"code": 403, "message": "You don't have permission to access api"}

    def has_permission(self, request, view):
        try:
            client_id = request.META[VERIFY_CLIENT_API_KEY]
            if client_id != settings.CLIENT_API_KEY:
                return False
            return True
        except Exception:
            return False


class AuthRequired(BaseException):
    """
    Allows access only to authenticated users.
    """
    message = {"code": 400, "message": "Invalid token. Please log in again."}

    def has_permission(self, request, view):
        try:
            # try:
            #     client_id = request.META[VERIFY_CLIENT_API_KEY]
            #     if client_id != settings.CLIENT_API_KEY:
            #         self.message = {"code": 403, "message": "You don't have permission to access api"}
            #         return False
            # except Exception:
            #     self.message = {"code": 403, "message": "You don't have permission to access api"}
            #     return False

            access_token = request.META.get(HTTP_AUTHORIZATION).split(" ")[1]
            from personal.models import Personal
            user_id = Personal.decode_auth_token(access_token)
            if user_id > 0:
                return True
            elif user_id == -3:
                self.message = {"code": 400, "message": "Invalid token. Please log in again."}
                return False
            elif user_id == -2:
                self.message = {"code": 400, "message": "Signature expired. Please log in again."}
                return False
            else:
                self.message = {"code": 400, "message": "Token blacklisted. Please log in again."}
                return False
        except Exception:
            return False
