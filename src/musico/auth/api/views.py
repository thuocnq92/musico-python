# Create your views here.
from base.permissions import AuthRequired
from rest_framework import status
from rest_framework.views import APIView
from rest_framework.response import Response
from personal.models import Personal, BlacklistToken
from .serializers import PersonalSerializer, PersonalDetailSerializer
from base.api.exceptions import (LoginAccountDisabled,
                                 LoginAccountNotActive,
                                 LoginWrong,
                                 LoginValidate,
                                 LoginEmailNotFound,
                                 MusicoBadRequest,
                                 PersonalCreateUserExp)

HTTP_AUTHORIZATION = 'HTTP_AUTHORIZATION'


class RegisterView(APIView):
    # permission_classes = (AuthRequired,)

    def post(self, request, *args, **kwargs):

        serialized = PersonalSerializer(data=request.data)
        if serialized.is_valid():
            serialized.save()
            return Response(serialized.data, status=status.HTTP_201_CREATED)
        else:
            return Response(serialized._errors, status=status.HTTP_400_BAD_REQUEST)

class LoginView(APIView):

    def post(self, request):
        password = request.data.get("password")
        user = Personal.objects.filter(email=request.data.get("email")).first()
        if user is not None:
            credentials = {
                'email': user.email,
                'password': password
            }
            if all(credentials.values()):

                if user.check_password(password):
                    if not user.active:
                        raise LoginAccountNotActive

                    return Response({
                        "success": True,
                        "data": {
                            "token": user.encode_auth_token(id=user.id),
                        },
                        "message": "Login success !"
                    })
                else:
                    raise LoginWrong

            else:
                raise LoginValidate

        else:
           raise LoginEmailNotFound


class RefreshTokenView(APIView):
    permission_classes = (AuthRequired,)

    def post(self, request, format=None):
        try:
            access_token = request.META.get(HTTP_AUTHORIZATION).split(" ")[1]
            user_id = Personal.decode_auth_token(access_token)

            user = Personal.objects.get(id=user_id)

            return Response({
                "result": {
                    "code": 200,
                    "message": "Refresh succeed",
                    "token": user.encode_auth_token(id=user_id),
                }
            })
        except Exception:
            raise PersonalCreateUserExp


class LogoutView(APIView):
    permission_classes = (AuthRequired,)

    def post(self, request):
        data = request.data
        access_token = request.META.get(HTTP_AUTHORIZATION).split(" ")[1]

        try:
            BlacklistToken.objects.create(token=access_token)
        except Exception:
            raise MusicoBadRequest('Logout fail!')

        return Response({"result": {"code": 201, "message": "succeed"}})


class PersonalDetailView(APIView):
    permission_classes = (AuthRequired,)

    def get(self, request, format=None):
        access_token = request.META.get(HTTP_AUTHORIZATION).split(" ")[1]
        user_id = Personal.decode_auth_token(access_token)

        user = Personal.objects.get(id=user_id)
        serializer = PersonalDetailSerializer(user).data

        return Response({'results': {
            "data": serializer,
        }})
