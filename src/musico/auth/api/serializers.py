from rest_framework import serializers
from rest_framework_jwt.settings import api_settings
from personal.models import Personal


class PersonalSerializer(serializers.ModelSerializer):
    password = serializers.CharField(write_only=True)

    class Meta:
        model = Personal
        fields = ('firstname', 'lastname', 'email', 'password')

    def create(self, validated_data):
        user = Personal.objects.create(email=validated_data['email'],
                                       firstname=validated_data['firstname'],
                                       lastname=validated_data['lastname'],
                                       )
        user.make_password(validated_data['password'])
        user.save()
        return user

class PersonalDetailSerializer(serializers.ModelSerializer):
    name = serializers.SerializerMethodField()

    class Meta:
        model = Personal
        fields = [
            'email',
            'firstname',
            'lastname',
            'name',
            'region',
            'language',
            'avatar',
            'background',
        ]

    def get_name(self, obj):
        return '%s %s' % (obj.firstname, obj.lastname)
