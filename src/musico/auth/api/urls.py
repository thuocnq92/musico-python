# snippets/urls.py
from django.conf.urls import url
from rest_framework.urlpatterns import format_suffix_patterns
from django.urls import path
from .views import RegisterView, LoginView, RefreshTokenView, LogoutView, PersonalDetailView

urlpatterns = [
    path('register', RegisterView.as_view(), name='register'),
    path('login', LoginView.as_view(), name='login'),
    path('refresh_token', RefreshTokenView.as_view(), name='refresh_token'),
    path('logout', LogoutView.as_view(), name='logout'),
    path('profile', PersonalDetailView.as_view(), name='profile'),
]

urlpatterns = format_suffix_patterns(urlpatterns)