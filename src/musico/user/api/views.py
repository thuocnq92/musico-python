import json

from django.contrib.auth.models import User, Group
from rest_framework import generics, pagination, viewsets
from .serializers import UserSerializer, GroupSerializer

# Create your views here.
from rest_framework.response import Response
from rest_framework.exceptions import ValidationError
from rest_framework.views import APIView
from django.db.models import Q
from base.permissions import AuthRequired

class UserViewSet(APIView):
    """
    API endpoint that allows users to be viewed or edited.
    """

    def get(self, request, format=None):
        permission_classes = (AuthRequired,)

        users = User.objects.all().order_by('-date_joined')
        serializer = UserSerializer(users, many=True).data

        return Response({'results': {
            "data": serializer
        }})


class GroupViewSet(APIView):
    """
    API endpoint that allows groups to be viewed or edited.
    """
    def get(self, request, format=None):

        groups = Group.objects.all()
        serializer = GroupSerializer(groups, many=True).data

        return Response({'results': {
            "data": serializer
        }})