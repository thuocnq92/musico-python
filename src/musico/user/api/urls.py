# snippets/urls.py
from django.conf.urls import url
from rest_framework.urlpatterns import format_suffix_patterns
from django.urls import path
from .views import UserViewSet, GroupViewSet

urlpatterns = [
    path('', UserViewSet.as_view(), name='list-user'),
    path('group', GroupViewSet.as_view(), name='list-group')
]

urlpatterns = format_suffix_patterns(urlpatterns)