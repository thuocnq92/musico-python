import datetime
import json
import jwt

from django.db import models
from django.conf import settings
from django.contrib.auth.hashers import make_password, check_password

DEACTIVE = 0
ACTIVE = 1
TOKEN_BLACKLIST = -1
TOKEN_EXPIRED = -2
TOKEN_INVALID = -3

REGION_DEFAULT = 'Singapore'
LANGUAGE_DEFAULT = 'en'
RATE_DEFAULT = '0.00'

# Create your models here.
class Personal(models.Model):
    username = models.CharField(max_length=50, null=True, blank=True)
    email = models.CharField(max_length=200, null=False, blank=False, unique=True)
    email_code = models.CharField(max_length=10, null=True, blank=True)
    expired_email_at = models.DateTimeField(auto_now=True, null=True, blank=True)
    firstname = models.CharField(max_length=20, null=True, blank=True)
    lastname = models.CharField(max_length=20, null=True, blank=True)
    password = models.CharField(max_length=200, null=False, blank=False)
    region = models.CharField(max_length=200, null=True, blank=True, default=REGION_DEFAULT)
    language = models.CharField(max_length=200, null=True, blank=True, default=LANGUAGE_DEFAULT)
    avatar = models.CharField(max_length=128, null=True, blank=True)
    background = models.CharField(max_length=128, null=True, blank=True)
    phone = models.CharField(max_length=20, null=True, blank=True, unique=True)
    otp = models.CharField(max_length=10, null=True, blank=True)
    expired_phone_at = models.DateTimeField(auto_now=True, null=True, blank=True)
    active_phone = models.BooleanField(default=False)
    active = models.BooleanField(default=False)
    signup_token = models.CharField(max_length=20, null=True, blank=True)
    forget_token = models.CharField(max_length=200, null=True, blank=True)
    expired_forget_at = models.DateTimeField(auto_now=True, null=True, blank=True)
    provider = models.CharField(max_length=20, null=True, blank=True)
    provider_id = models.CharField(max_length=200, null=True, blank=True)
    email_temp = models.CharField(max_length=200, null=True, blank=True)
    phone_temp = models.CharField(max_length=10, null=True, blank=True)
    stripe_id = models.CharField(max_length=200, null=True, blank=True)
    card_brand = models.CharField(max_length=200, null=True, blank=True)
    card_last_four = models.CharField(max_length=4, null=True, blank=True)
    stripe_account_id = models.CharField(max_length=200, null=True, blank=True)
    account_card_last_four = models.CharField(max_length=4, null=True, blank=True)
    avg_rate_customer = models.DecimalField(max_digits=5, decimal_places=2, blank=True, null=True, default=RATE_DEFAULT)

    class Meta:
        db_table = 'tbl_personal'
        verbose_name = "Personal"
        verbose_name_plural = "Personal"
        indexes = [
        ]

    def set_password(self, password):
        self.password = make_password(password)

    def check_password(self, password):
        return check_password(password, self.password)

    def encode_auth_token(self, id):
        try:
            payload = {
                'exp': datetime.datetime.utcnow() + datetime.timedelta(days=settings.JWT_DAYS_DELTA,
                                                                       seconds=settings.JWT_SECONDS_DELTA),
                'iat': datetime.datetime.utcnow(),
                'sub': id
            }
            return jwt.encode(
                payload=payload,
                key=settings.JWT_SECRET_KEY,
                algorithm=settings.JWT_ALGORITHM
            )
        except Exception as e:
            return e

    def create_refesh_token(self, id):
        try:
            payload = {
                'exp': datetime.datetime.utcnow() + datetime.timedelta(days=settings.JWT_DAYS_REFRESH_DELTA,
                                                                       seconds=settings.JWT_SECONDS_REFRESH_DELTA),
                'iat': datetime.datetime.utcnow(),
                'sub': id
            }
            return jwt.encode(
                payload=payload,
                key=settings.JWT_SECRET_KEY,
                algorithm=settings.JWT_ALGORITHM
            )
        except Exception as e:
            return e

    @staticmethod
    def decode_auth_token(auth_token):
        try:
            payload = jwt.decode(auth_token, settings.JWT_SECRET_KEY)
            is_blacklisted_token = BlacklistToken.check_blacklist(auth_token)
            if is_blacklisted_token:
                return TOKEN_BLACKLIST
            else:
                return payload['sub']
        except jwt.ExpiredSignatureError:
            return TOKEN_EXPIRED
        except jwt.InvalidTokenError:
            return TOKEN_INVALID

class BlacklistToken(models.Model):
    token = models.TextField(null=False)
    blacklisted_on = models.DateTimeField(auto_now_add=True)

    @staticmethod
    def check_blacklist(auth_token):
        # check whether auth token has been blacklisted
        res = BlacklistToken.objects.filter(token=auth_token).first()
        if res:
            return True
        else:
            return False