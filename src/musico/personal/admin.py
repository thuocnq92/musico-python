from django.contrib import admin
from .models import Personal

# Register your models here.
class PersonalAdmin(admin.ModelAdmin):
    model = Personal
    list_display = ['email', 'fullname', 'password']
    list_filter = ['email', 'firstname', 'lastname']
    exclude = ['username']

    def fullname(self, obj):
        if obj.firstname is None or obj.lastname is None:
            return '-'

        return ("{0} {1}".format(obj.firstname, obj.lastname))


admin.site.register(Personal, PersonalAdmin)

