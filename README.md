How to install and run this project:

$ cd musico-python
$ pip install virtualenv
$ virtualenv env
$ source env/bin/activate  # On Windows use `env\Scripts\activate`
$ cd src/musico
$ pip install -r requirements.txt
$ python manage.py makemigrations
$ python manage.py migrate

****run server****
$ python manage.py runserver